# Created on 19.01.2021
# Author: RV

from pykinect2 import PyKinectV2
from pykinect2 import PyKinectRuntime
import ctypes
import pygame

class KinectController(object):
    
    def __del__(self):
        self._kinect.close()
        #self.pygame.quit()
    
    def __init__(self):
        print("KinectController initialized.")
        pygame.init()

        # Used to manage how fast the screen updates.
        self._clock = pygame.time.Clock()

        # Set the width and height of the screen [width, height].
        self._infoObject = pygame.display.Info()
        self._screen = pygame.display.set_mode((self._infoObject.current_w >> 1, self._infoObject.current_h >> 1), 
                                               pygame.HWSURFACE|pygame.DOUBLEBUF|pygame.RESIZABLE, 32)

        pygame.display.set_caption("Rack Recording")

        # Loop until the user clicks the close button.
        self._done = False

        # Used to manage how fast the screen updates.
        self._clock = pygame.time.Clock()

        # Runtime object, we want only color and body frames.
        self._kinect = PyKinectRuntime.PyKinectRuntime(PyKinectV2.FrameSourceTypes_Color)

        # Back buffer surface for getting color frames, 32bit color, width and height equal to the color frame size.
        self._frame_surface = pygame.Surface((self._kinect.color_frame_desc.Width, self._kinect.color_frame_desc.Height), 0, 32)

    def draw_color_frame(self, frame, target_surface):
        target_surface.lock()
        address = self._kinect.surface_as_array(target_surface.get_buffer())
        ctypes.memmove(address, frame.ctypes.data, frame.size)
        del address
        target_surface.unlock()

    def record_image(self, image_path):
           
        # Start main event loop.
        for event in pygame.event.get():
            if event.type == pygame.QUIT: # ...if user clicked close...
                self._done = True # ...we are done so we exit this loop...
            elif event.type == pygame.VIDEORESIZE: # ...window was resized...
                self._screen = pygame.display.set_mode(event.dict['size'],pygame.HWSURFACE|pygame.DOUBLEBUF|pygame.RESIZABLE, 32)
                
        # Get frames and draw.
        if self._kinect.has_new_color_frame():
            frame = self._kinect.get_last_color_frame()
            self.draw_color_frame(frame, self._frame_surface)
            frame = None

        # Copy back buffer surface pixels to the screen, resize it if needed and keep aspect ratio.
        # Screen size may be different from Kinect's color frame size.
        h_to_w = float(self._frame_surface.get_height()) / self._frame_surface.get_width()
        target_height = int(h_to_w * self._screen.get_width())
        surface_to_draw = pygame.transform.scale(self._frame_surface, (self._screen.get_width(), target_height));
        self._screen.blit(surface_to_draw, (0,0))
        surface_to_draw = None
        pygame.display.update()

        # Update the screen.
        pygame.display.flip()
        
        # Limit to 60 frames per second.
        self._clock.tick(60)
        
        # Record image.
        pygame.image.save(self._screen, image_path)