# Created on 20.01.2021
# Author: NN

from Objects.WayPoint import WayPoint
from Objects.Grid_3x3 import Grid_3x3
from Objects.Grid_5x5 import Grid_5x5

class GridCalculator(object):
    
    list_WayPoints_3D = []
    
    def __init__(self):
        print("GridCalculator initialized.")

    def calculate_grid(self, GridDimension, GridGap, GridDistance, GrippingPoint, AngleStep):

        if GridDimension == 3:
            grid = Grid_3x3()
        elif GridDimension == 5:
            grid = Grid_5x5()
            
        list_WayPoints = grid.get()
        
        for WayPoint in list_WayPoints:
            WayPoint.multiplyAngle(AngleStep)
            WayPoint.multiplyGab(GridGap)
            WayPoint.setDistance(GridDistance)
            WayPoint.addGrippingPoint(GrippingPoint[0],GrippingPoint[1],GrippingPoint[2],GrippingPoint[3],GrippingPoint[4],GrippingPoint[5])
            
        return list_WayPoints
    