# Created on 20.01.2021
# Author: NNr: Administrator

from Objects.WayPoint import WayPoint

class Grid_3x3(object):
    
    list_WayPoints = []

    def get(self):
        return self.list_WayPoints

    def __init__(self):
        WayPoint_1 = WayPoint()
        WayPoint_1.set(-1, 0, 1, 0, 0, 0)
        self.list_WayPoints.append(WayPoint_1)
        WayPoint_2 = WayPoint()
        WayPoint_2.set(0, 0, 1, 0, 0, 0)
        self.list_WayPoints.append(WayPoint_2)
        WayPoint_3 = WayPoint()
        WayPoint_3.set(1, 0, 1, 0, 0, 0)
        self.list_WayPoints.append(WayPoint_3)
        WayPoint_4 = WayPoint()
        WayPoint_4.set(1, 0, 0, 0, 0, 0)
        self.list_WayPoints.append(WayPoint_4)
        WayPoint_5 = WayPoint()
        WayPoint_5.set(0, 0, 0, 0, 0, 0)
        self.list_WayPoints.append(WayPoint_5)
        WayPoint_6 = WayPoint()
        WayPoint_6.set(-1, 0, 0, 0, 0, 0)
        self.list_WayPoints.append(WayPoint_6)
        WayPoint_7 = WayPoint()
        WayPoint_7.set(-1, 0, -1, 0, 0, 0)
        self.list_WayPoints.append(WayPoint_7)
        WayPoint_8 = WayPoint()
        WayPoint_8.set(0, 0, -1, 0, 0, 0)
        self.list_WayPoints.append(WayPoint_8)
        WayPoint_9 = WayPoint()
        WayPoint_9.set(1, 0, -1, 0, 0, 0)
        self.list_WayPoints.append(WayPoint_9)  