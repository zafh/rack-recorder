# Created on 20.01.2021
# Author: NN

import socket

class RobotController(object):
    
    FORMAT = 'utf-8'
    velocity = 0.2

    def __del__(self):
        self.c.close()
        self.s.close()

    def __init__(self):
        HOST = "192.168.101.42"
        PORT = 30000
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.s.bind((HOST, PORT)) 
        self.s.listen(1) 
        print('Socket server has been started.')
        self.c, addr = self.s.accept()
        print('Client has been connected (' + str(addr) + ')')
        print("RobotController initialized.")

    def setFormat(self, form):
        self.FORMAT = form
        
    def setVelocity(self, velocity):
        self.velocity = velocity

    def move(self, WayPoint, Mode):   
        #if Mode == "C":
        wp = WayPoint.getRobotCoordinate()
        #elif Mode == "Rx":
            #wp = WayPoint.getRx()
        #elif Mode == "Rz": 
            #wp = WayPoint.getRz()       
        self.c.send(wp.encode(self.FORMAT))      
        response= self.c.recv(1024)
        print(response.decode('utf-8'))
 
        return True