# Created on 20.01.2021
# Author: RV

class WayPoint(object):
    
    C = []
    
    def __init__(self):
        self.C = [0,0,0,0,0,0]
        
    def getC(self):
        return self.C
        
    def set(self, x, y, z, Rx, Ry, Rz):
        self.C = [x, y, z, Rx, Ry, Rz]
        
    def setDistance(self, d):
        self.C[1] = -d
        
    def multiplyGab(self, g):
        self.C[0] = round(self.C[0] * g, 5)
        self.C[2] = round(self.C[2] * g, 5)
        
    def multiplyAngle(self, angle):
        self.C[3] = angle * self.C[0]
        self.C[5] = angle * self.C[2] 
        
    def addGrippingPoint(self, x, y, z, Rx, Ry, Rz):
        self.C[0] = round(self.C[0] + x, 5)
        self.C[1] = round(self.C[1] + y, 5)
        self.C[2] = round(self.C[2] + z, 5)
        self.C[3] = round(self.C[3] + Rx, 5)
        self.C[4] = round(self.C[4] + Ry, 5)
        self.C[5] = round(self.C[5] + Rz, 5)
        
    def getRobotCoordinate(self):
        c = "("
        c = c + str(self.C[0]) + ","
        c = c + str(self.C[1]) + ","
        c = c + str(self.C[2]) + ","
        c = c + str(self.C[3]) + ","
        c = c + str(self.C[4]) + ","
        c = c + str(self.C[5]) + ")"  
        return c