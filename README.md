# Rack Recorder #

### Beschreibung ###
Der Rack Recorder ermöglicht die automatisierte Aufnahme von Bildern im Regal der PickingStation im Logistiklabor der Technischen Hochschule Ulm für die Generierung von Bilddaten durch eine Microsoft Kinect. Die Bilder werden für das  Training von Neuronalen Netzen zur Objekterkennung verwendet.

### Systemanforderungen ###
- Die Software wurde mit Python 3.8.5 entwickelt.
- Für den Zugriff auf die Kinect werden die Bibliotheken 'pykinect2', 'ctypes' und 'pygame' verwendet.
- Der UR5e der PickingStation wird über eine Socket-Verbindung kontrolliert. Diese wird durch das Paket 'socket' realisiert.
- Für die Berechnung der Positionen und Datenobjekte werden 'numpy' und 'math' benutzt.

### Software (Python) ###
Die Software basiert auf den folgenden Modulen: GridCalculator, KinectController und RobotController. Die Module werden durch eine zentrale Methode gesteuert.

#### GridCalculator ####
Der GridCalculator berechnet auf Grundlage von Parametern für Größe und Ausprägung der Kamerapositionen die Koordinaten für den Roboter, die nacheinander angefahren werden.

#### KinectController ####
Der KinectController triggert die Aufnahme eines Bildes, d.h. er verbindet sich mit der Kinect und liest den Datenstrom für ein 2D-Bild aus. Die Bilder werden lokal gespeichert.

#### RobotController ####
Der RobotController stellt die Socket-Verbindung mit dem UR5e her und übermittelt die Koordinaten für die Bewegung.

### Software (UR5e) ###
Ein UR-Skript wird auf dem UR5e implementiert, das sich mit einem vorgegebenen Socket-Server verbindet und auf Koordinaten wartet. Werden diese bereitgestellt bewegt sich der Roboter zu dieser Position, gibt eine Rückmeldung und wartet wieder auf weitere Befehle.

### Acknowledgment ###
This work is part of the project “ZAFH Intralogistik”, funded by the European Regional Development Fund and the Ministry of Science, Research and Arts of Baden-Württemberg, Germany (F.No. 32-7545.24-17/3/1). It is also done within the post graduate school “Cognitive Computing in Socio-Technical Systems“ of Ulm University of Applied Sciences and Ulm University, which is funded by Ministry for Science, Research and Arts of the State of Baden-Württemberg, Germany.