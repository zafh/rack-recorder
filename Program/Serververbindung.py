
import socket
import time 
import math

d2r = lambda x: math.pi/180.0 *x


HOST = "192.168.101.42" 
PORT = 30000
print ("Starting Program")
count = 0


#Ablageorte 
Koordinate_1 = "(-0.490, 0.425, 0.5875, 2.421, 2.409, 2.421)"
roll = d2r(45)
pitch = d2r(45)
yaw =d2r(0)
rpy = "["+str(roll)+","+str(pitch)+","+str(yaw)+"]"

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT)) 
s.listen(1) 
#s.settimeout(None)

while (count <4):
    #Verbindung zum Roboter wird hergestellt
    c, addr = s.accept() 
    
    #senden der Koordinaten. Zuordnung einer Variable in Polyscope.
    c.send((Koordinate_1).encode('utf-8'))
    c.send((rpy).encode('utf-8'))
    #c.send((roll).encode('utf-8'))
    #c.send((pitch).encode('utf-8'))
    #c.send((yaw).encode('utf-8'))
    
    count = count + 1
    
    print ("")
    
    print ("Position", count)
    
    msg= c.recv(1024)

    print(msg.decode('utf-8'))
    
    c.close()
s.close()

print ("Kommissioniervorgang beendet!")