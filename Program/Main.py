# Created on 19.01.2021
# Author: RV

from Objects.KinectController import KinectController
from Objects.RobotController import RobotController
from Objects.GridCalculator import GridCalculator
from Objects.WayPoint import WayPoint
import numpy

__main__ = "Rack Recording"

# Initialize objects
KinectController = KinectController()
RobotController = RobotController()
GridCalculator = GridCalculator()

# Article
Article = "Object"

# Set grid parameters
GridDimension = 3
GridGap = 0.05
GridDistance = 0.1
AngleStep = numpy.arctan(GridDistance/(((GridDimension-1)/2) * GridGap)) #/((GridDimension-1)/2)

# Set movement parameters
Velocity = 0.3
RobotController.setVelocity(Velocity)

# Set gripping/starting point parameters
GrippingPoint = [-0.19126, 0.46460, 0.49854, 0.030, 2.232, 2.232]
WayPointStart = WayPoint()
WayPointStart.set(GrippingPoint[0], GrippingPoint[1]-GridDistance, GrippingPoint[2], GrippingPoint[3], GrippingPoint[4], GrippingPoint[5])

# Move to gripping/starting point
RobotController.move(WayPointStart, "C")

ImageCounter = 1

# Calculate grid
WayPointsGrid = GridCalculator.calculate_grid(GridDimension, GridGap, GridDistance, GrippingPoint, AngleStep)

# Loop calculated list and move robot to every way point while recording images
for WayPoint in WayPointsGrid:
    
    if RobotController.move(WayPoint, "C"):
        #if RobotController.move(WayPoint, "Rx"):
            #if RobotController.move(WayPoint, "Rz"):
        KinectController.record_image("C:/RackRecording/Images/img_" + Article + "_" + str(ImageCounter) + ".png")
        print("Image has been recorded.")
        ImageCounter += 1
    
RobotController.move(WayPointStart, "C")

# Delete objects
KinectController.__del__()
RobotController.__del__()

# Finish program
print("Image recording has been finished.")